# Python imports here
import json
import time
import sys

# 3rd party Imports Here
import requests
import grequests
from requests.auth import HTTPBasicAuth


# Project imports here
from config import URL_ORG_REPO, URL_REPO_CONTRIB
from config import GITHUB_USERNAME, GITHUB_ACCESS_TOKEN

auth = HTTPBasicAuth(GITHUB_USERNAME, GITHUB_ACCESS_TOKEN)


def get_repos(org, sort_by, limit):
    search_url = URL_ORG_REPO.format(org=org, sort_by=sort_by)
    req = requests.get(search_url, auth=auth)
    repos = json.loads(req.content).get('items')
    return repos[:limit] if len(repos) > limit else repos


def get_contributors(reqs, limit):
    contributor_reqs = grequests.map(reqs)
    print contributor_reqs
    contributors = []
    for contributor_req in contributor_reqs:
        content = json.loads(contributor_req.content)
        content = content[:limit] if len(content) > limit else content
        contributors.append(content)
    return contributors


def prepare_response(repos, all_contributors):
    response = {}
    for repo, contributors in zip(repos, all_contributors):
        response[repo.get('name')] = []
        for contributor in contributors:
            response[repo.get('name')].append({'name': contributor['login'],
                                               'contributions': contributor['contributions']})
    return response


def get_grequest(org, repo):
    return grequests.get(URL_REPO_CONTRIB.format(org=org, repo=repo), auth=auth)


if __name__ == '__main__':
    start_time = time.time()
    org = sys.argv[1]
    n = int(sys.argv[2])
    m = int(sys.argv[2])
    repos = get_repos(org, 'forks', n)
    reqs = []
    for repo in repos:
        repo_name = repo.get('name')
        # print repo_name
        print URL_REPO_CONTRIB.format(org=org, repo=repo_name)
        reqs.append(grequests.get(URL_REPO_CONTRIB.format(org=org, repo=repo_name), auth=auth))

    all_contributors = get_contributors(reqs, limit=m)
    output = prepare_response(repos, all_contributors)
    print json.dumps(output)

    print("********END*********:", time.time()-start_time)


