import os

URL_ORG_REPO = "https://api.github.com/search/repositories?" \
               "q={org}+user:{org}&sort={sort_by}&order=desc"

URL_REPO_CONTRIB = "https://api.github.com/repos/{org}/{repo}/contributors"

GITHUB_USERNAME = os.environ['GITHUB_USERNAME']
GITHUB_ACCESS_TOKEN = os.environ['GITHUB_ACCESS_TOKEN']

