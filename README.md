## Synopsis

Almabase Github Task

## Code Example

To run command line:

```
#!python
python utils.py google 10 5


```
To access web api first run the flask app:

cd into directory and run 

```
#!python
python run.py

```

API:
```
http://localhost:5000/search/?org=google&n=4&m=4

```
Here n and m are optional parameters


## Installation

```
pip install -r requirements.txt


```