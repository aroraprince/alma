from utils import get_repos, get_contributors, prepare_response, get_grequest

from flask import Flask, request
from flask import jsonify
app = Flask(__name__)


@app.route('/search/', methods=['GET'])
def search_github():
    if 'org' in request.args:
        org = request.args['org']
        n = int(request.args.get('n', 4))
        m = int(request.args.get('m', 4))
    else:
        return "required parameters `org`", 400

    repos = get_repos(org, 'forks', n)
    reqs = []
    for repo in repos:
        repo_name = repo.get('name')
        reqs.append(get_grequest(org, repo_name))

    all_contributors = get_contributors(reqs, limit=m)
    response = prepare_response(repos, all_contributors)
    return jsonify(response), 200

if __name__ == "__main__":
    app.run(host='0.0.0.0')
